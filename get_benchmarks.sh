#!/bin/bash

source phones.sh

echo "Clearing device logs..."
adb -s $PHONE_1 logcat -c
adb -s $PHONE_2 logcat -c

echo "Capturing benchmarks..."

{(adb -s $PHONE_1 logcat | grep "I org.example.anonymous.innercircle" > device_1.log) &}
echo $!
adb -s $PHONE_2 logcat | grep "I org.example.anonymous.innercircle" > device_2.log

