package org.example.anonymous.innercircle;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import org.example.anonymous.innercircle.services.LocationService;

public class BackgroundLocationService extends BroadcastReceiver {
    private static final String TAG = BackgroundLocationService.class.getName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "Receiver started");
        Intent background = new Intent(context, LocationService.class);
        context.startService(background);
    }
}
