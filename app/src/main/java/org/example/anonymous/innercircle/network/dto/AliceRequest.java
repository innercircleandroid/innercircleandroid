package org.example.anonymous.innercircle.network.dto;

import org.example.anonymous.innercircle.crypto.elgamal.CipherText;
import org.example.anonymous.innercircle.crypto.LocationProximity;
import org.example.anonymous.innercircle.crypto.elgamal.PublicKey;
import org.example.anonymous.innercircle.crypto.elgamal.SecretKey;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;
import java.util.ArrayList;

public class AliceRequest {

    private static final String TAG = AliceRequest.class.getName();

    public String makeJsonObject(CipherText[] ciphertexts, PublicKey pk, int radius, ArrayList<String> recpName, String sender) {
        String[] names = new String[recpName.size()];
        names = recpName.toArray(names);
        JSONObject jsonReq = new JSONObject();
        JSONObject jsonF = new JSONObject();
        JSONObject jsonObj = new JSONObject();
        try {
            jsonReq.put("A0.C0", ciphertexts[0].C0.toString());
            jsonReq.put("A0.C1", ciphertexts[0].C1.toString());
            jsonReq.put("A1.C0", ciphertexts[1].C0.toString());
            jsonReq.put("A1.C1", ciphertexts[1].C1.toString());
            jsonReq.put("A2.C0", ciphertexts[2].C0.toString());
            jsonReq.put("A2.C1", ciphertexts[2].C1.toString());
            jsonReq.put("P", pk.getP().toString());
            jsonReq.put("G", pk.getG().toString());
            jsonReq.put("Y", pk.getY().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            //TODO: make Bob id not hardcoded.
            jsonReq.put("Sender_ID", sender);// Alice ID
            jsonF.put("Recepient_ID", new JSONArray(names)); // Arrays of recp ID
            jsonReq.put("Radius", radius);//radius

            jsonF.put("Cred", jsonReq);// add all the keys in the message
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            jsonObj.put("Requests", jsonF);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObj.toString();
    }
}
