package org.example.anonymous.innercircle.crypto.elgamal;

import java.math.BigInteger;

public class CipherText {
    public final BigInteger C0;
    public final BigInteger C1;


    public CipherText(BigInteger C0, BigInteger C1) {
        this.C0 = C0;
        this.C1 = C1;
    }

}
