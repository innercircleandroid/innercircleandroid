package org.example.anonymous.innercircle.crypto;

import org.example.anonymous.innercircle.crypto.elgamal.CipherText;
import org.example.anonymous.innercircle.crypto.elgamal.ElgamalCrypto;
import org.example.anonymous.innercircle.crypto.elgamal.PublicKey;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SumOfSquares {
    public static final Map<Integer, List<Integer>> map = new HashMap<>();
    public static final Map<Integer, CipherText> negative = new HashMap<>();

    public static List<Integer> getSumOfSquares(int max) {
        return map.get(max);
    }

    public static CipherText getInverse(int i) {
        return negative.get(i);
    }

    public static void initializeSumOfSquares(PublicKey publicKey) {
        int max = 125;
        for (int r = 0; r < max; r += 25) {
            List<Integer> sumOfSquares = new ArrayList<>();
            for (int i = 0; i <= r; i++) {
                int limit = (int) Math.ceil(Math.sqrt(Math.pow(r, 2) - Math.pow(i, 2)));
                for (int j = i; j <= limit; j++) {
                    int sum_of_square = (int) (Math.pow(j, 2) + Math.pow(i, 2));
                    if (!sumOfSquares.contains(sum_of_square))
                        sumOfSquares.add(sum_of_square);
                }
            }

            if (r != 0) {
                int sum_of_square = (int) (2 * Math.pow(r, 2));
                sumOfSquares.add(sum_of_square);
            }

            Collections.sort(sumOfSquares);
            map.put(r, sumOfSquares);
        }

        ElgamalCrypto elgamalCrypto = new ElgamalCrypto();

        final CipherText zero = elgamalCrypto.encryption(publicKey, new BigInteger(String.valueOf(0)));
        for (List<Integer> sumOfSquares : SumOfSquares.map.values()) {
            for (Integer sumOfSquare : sumOfSquares) {
                if (!SumOfSquares.negative.containsKey(sumOfSquare)) {
                    final CipherText encryptedSum = elgamalCrypto.encryption(publicKey, new BigInteger(String.valueOf(sumOfSquare)));
                    final CipherText negativeSum = elgamalCrypto.subtract(publicKey, zero, encryptedSum);
                    SumOfSquares.negative.put(sumOfSquare, negativeSum);
                }
            }
        }
    }
}
