package org.example.anonymous.innercircle.crypto;

import android.util.Log;

import org.example.anonymous.innercircle.crypto.elgamal.CipherText;
import org.example.anonymous.innercircle.crypto.elgamal.ElgamalCrypto;
import org.example.anonymous.innercircle.crypto.elgamal.PublicKey;
import org.example.anonymous.innercircle.crypto.elgamal.SecretKey;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class AliceProtocol extends LocationProximity {
    private static final String TAG = AliceProtocol.class.getName();

    public AliceProtocol(PublicKey publicKey, int x, int y) {
        super(publicKey, x, y);
    }

    public boolean InProx(ArrayList<CipherText> result, SecretKey secretK) {
        final long start = System.nanoTime();

        for (int i = 0; i < result.size(); i++) {
            Log.d(TAG, String.format("location proximity %.2f", (100.0f * i) / result.size()));
            CipherText cipher = result.get(i);
            if (elgamal.decrypt(Pk, secretK, cipher)) {
                Log.d(TAG, "location proximity. HOORAY!!!");
                return true;
            }
        }

        final long end = System.nanoTime();
        Log.i(TAG, String.format("InProx finished in time='%s' seconds", (end - start) / 1000000000.0d));
        return false;
    }

    public CipherText[] generateEncryptedLocation() {// publickey ,Alice  x-coordinate, Alice y-coordinate
        final long start = System.nanoTime();
        BigInteger xA2 = new BigInteger(String.valueOf(x)).pow(2);
        BigInteger yA2 = new BigInteger(String.valueOf(y)).pow(2);

        CipherText a0 = elgamal.encryption(Pk, xA2.add(yA2));
        CipherText a1 = elgamal.encryption(Pk, new BigInteger(String.valueOf(2 * x)));
        CipherText a2 = elgamal.encryption(Pk, new BigInteger(String.valueOf(2 * y)));

        CipherText[] cred = new CipherText[3];
        cred[0] = a0;
        cred[1] = a1;
        cred[2] = a2;

        final long end = System.nanoTime();
        Log.i(TAG, String.format("generateEncryptedLocation finished in time='%s' seconds", (end - start) / 1000000000.0d));
        return cred;
    }
}
