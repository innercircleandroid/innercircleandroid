package org.example.anonymous.innercircle.crypto.elgamal;

import android.os.Environment;

import org.example.anonymous.innercircle.crypto.SumOfSquares;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Random;


public class ElgamalCrypto {
    private final Random secureRandom = new SecureRandom();

    public CipherText encryption(PublicKey Pk, BigInteger m) {
        BigInteger X = Pk.getG().modPow(m, Pk.getP()); // additive

        BigInteger r = new BigInteger(1024, secureRandom);
        BigInteger C0 = Pk.getG().modPow(r, Pk.getP());
        BigInteger C1 = X.multiply(Pk.getY().modPow(r, Pk.getP())).mod(Pk.getP());

        return new CipherText(C0, C1);
    }

    public CipherText add(PublicKey Pk, CipherText cipherA, CipherText cipherB) {
        BigInteger C0 = (cipherA.C0.multiply(cipherB.C0).mod(Pk.getP()));
        BigInteger C1 = cipherA.C1.multiply(cipherB.C1).mod(Pk.getP());

        return new CipherText(C0, C1);
    }

    public CipherText subtract(PublicKey Pk, CipherText cipherA, CipherText cipherB) {
        BigInteger C0 = (cipherA.C0.multiply(cipherB.C0.modInverse(Pk.getP()))).mod(Pk.getP());
        BigInteger C1 = cipherA.C1.multiply(cipherB.C1.modInverse(Pk.getP())).mod(Pk.getP());

        return new CipherText(C0, C1);
    }

    public CipherText multWithNum(PublicKey Pk, CipherText cipherA, BigInteger num) {
        BigInteger C0 = (cipherA.C0.modPow(num, Pk.getP()));
        BigInteger C1 = (cipherA.C1.modPow(num, Pk.getP()));

        return new CipherText(C0, C1);
    }

    public boolean decrypt(PublicKey Pk, SecretKey secretKey, CipherText cipher) {
        BigInteger crmodp = (cipher.C0).modPow(secretKey.getX(), Pk.getP());
        BigInteger d = crmodp.modInverse(Pk.getP());
        BigInteger ad = d.multiply(cipher.C1).mod(Pk.getP());
        return ad.equals(BigInteger.ONE);

    }

    public int decryptText(PublicKey Pk, SecretKey secretKey, CipherText cipher) {
        BigInteger crmodp = (cipher.C0).modPow(secretKey.getX(), Pk.getP());
        BigInteger d = crmodp.modInverse(Pk.getP());
        BigInteger ad = d.multiply(cipher.C1).mod(Pk.getP());

        int i;
        for (i = 0; i < 100; i++) {
            if (Pk.getG().modPow(new BigInteger(String.valueOf(i)), Pk.getP()).equals(ad)) {
                break;
            }
        }
        return i;
    }


    public void readFromFile() {
        //Find the directory for the SD Card using the API
        //*Don't* hardcode "/sdcard"
        //File sdcard = Environment.getExternalStorageDirectory();

        //Get the text file
        File root = new File(Environment.getExternalStorageDirectory(), "Squares");
        File file = new File(root, "SumOfSquares.txt");

        //Read text from file
        StringBuilder text = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            br.close();
        } catch (IOException e) {
            //You'll need to add proper error handling here
        }


    }


    public int getTime() {
        Calendar c = Calendar.getInstance();
        int startMili = c.get(Calendar.MILLISECOND);
        int startSecond = c.get(Calendar.SECOND);
        int startMinute = c.get(Calendar.MINUTE);
        int startHour = c.get(Calendar.HOUR);
        return (startHour * 3600000) + (startMinute * 60000) + (startSecond * 1000) + startMili;
    }

    public KeyPair generateKeys() {
        BigInteger x = new BigInteger(1024, secureRandom);
        BigInteger p = BigInteger.probablePrime(1024, secureRandom); // prime
        BigInteger g = new BigInteger(1024, secureRandom); // generator
        BigInteger y = g.modPow(x, p);

        return new KeyPair(new PublicKey(p, g, y), new SecretKey(x));
    }

}

