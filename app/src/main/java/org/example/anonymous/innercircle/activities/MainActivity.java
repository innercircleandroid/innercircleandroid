package org.example.anonymous.innercircle.activities;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import org.example.anonymous.innercircle.Holder;
import org.example.anonymous.innercircle.R;
import org.example.anonymous.innercircle.crypto.SumOfSquares;
import org.example.anonymous.innercircle.crypto.elgamal.ElgamalCrypto;
import org.example.anonymous.innercircle.crypto.elgamal.KeyPair;
import org.example.anonymous.innercircle.crypto.elgamal.PublicKey;
import org.example.anonymous.innercircle.network.dto.MyResult;
import org.example.anonymous.innercircle.services.LocationService;
import org.example.anonymous.innercircle.services.ProximityRequestService;
import org.example.anonymous.innercircle.ui.CustomListAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getName();

    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 0;
    public static ArrayList<String> resultsArray = new ArrayList<>();
    public static PublicKey publicKey;//public key
    //SendDataTask data;
    public static MyResult resultReceiver = new MyResult(null);
    public static long startTime;
    public static int encryptionTime;
    public static ArrayList<String> requests;
    private ArrayList<String> contactList = new ArrayList<>();
    private ArrayList<Holder> itemList = new ArrayList<>();
    private CustomListAdapter listAdapter;
    private ListView listView;
    private String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ElgamalCrypto crypto = new ElgamalCrypto();
        KeyPair keyPair = crypto.generateKeys();
        publicKey = keyPair.getPublicKey();
        Log.d(TAG, "Initializing sum of squares");
        SumOfSquares.initializeSumOfSquares(publicKey);
        Log.d(TAG, "Done initializing sum of squares");
        storeSecretKey(keyPair);

        //Requesting permission to use user's location.
        //this is necessary since android API 23.
        //int permissionCheck = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);
        Intent alarm = new Intent(this, LocationService.class);
        alarm.putExtra("receiver", resultReceiver);
        startService(alarm);


        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);

            // MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION is an
            // app-defined int constant. The callback method gets the
            // result of the request.
        }

        requests = new ArrayList<>();


        SharedPreferences prefs = getSharedPreferences("UserCred", Context.MODE_PRIVATE);
        username = prefs.getString("Username", "");
        contactList.add("Alice");
        contactList.add("Bob");
        // contactList.add("Cyril");
        // contactList.add("David");
        // contactList.add("Ellen");
        // contactList.add("Fred");
        // contactList.add("Garry");
        // contactList.add("Henry");
        // contactList.add("Igor");
        // contactList.add("John");
        // contactList.add("Katherine");
        // contactList.add("Louise");
        // contactList.add("Marcus");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        toolbar.setTitle("Watch");
        setSupportActionBar(toolbar);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

        servicesConnected(this);

        //Sets up the spinner to show range options_________________________________________________
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.radius_options, android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerAdapter);

        //Sets up the Contact List__________________________________________________________________
        for (int i = 0; i < contactList.size(); i++) {
            Holder itemHolder = new Holder();
            itemHolder.setTitle(contactList.get(i));
            itemHolder.setColor(Color.WHITE);
            itemList.add(itemHolder);
        }

        listAdapter = new CustomListAdapter(this, itemList);
        listView = (ListView) findViewById(R.id.contact_list);
        listView.setAdapter(listAdapter);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted
                    Intent alarm = new Intent(this, LocationService.class);
                    alarm.putExtra("receiver", resultReceiver);
                    startService(alarm);

                } else {
                    // permission denied.
                }
                return;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(Menu.NONE, R.id.app_settings, Menu.NONE, R.string.settings_title);
        menu.add(Menu.NONE, R.id.map_tab, Menu.NONE, R.string.map_title);
        menu.add(Menu.NONE, R.id.location_requests, Menu.NONE, R.string.request_title);

        return true;
    }

    public void locate(View view) {
        Intent locationServiceIntent = new Intent(this, LocationService.class);
        locationServiceIntent.putExtra("receiver", resultReceiver);
        startService(locationServiceIntent);

        resultsArray.clear();
        ArrayList<String> selectedContacts = new ArrayList<>();
        for (int i = 0; i < itemList.size(); i++) {
            if (itemList.get(i).getColor() == Color.CYAN) {
                selectedContacts.add(itemList.get(i).getTitle());
            }
        }

        if (selectedContacts.size() == 0) {
            new AlertDialog.Builder(this)
                    .setTitle("Warning")
                    .setMessage("Please select at least one contact before locating their proximity.")
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //do nothing
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();

        } else {

            Spinner spinner = (Spinner) findViewById(R.id.spinner);
            int radius = Integer.parseInt(spinner.getSelectedItem().toString());

            Log.d(TAG, "numberOfContacts" + selectedContacts.size());
            int xA = resultReceiver.makePrecsion()[0];
            int yA = resultReceiver.makePrecsion()[1];
            Log.d(TAG, "Sent X " + xA + ", Sent Y " + yA);

            String[] contacts = new String[selectedContacts.size()];
            contacts = selectedContacts.toArray(contacts);
            String contactsArray = Arrays.toString(contacts);
            Log.d(TAG, "Array: " + contactsArray);

            ProximityRequestService.setup(getApplicationContext(), publicKey, xA, yA, radius, selectedContacts);
            ProximityRequestService.getInstance().initiateRequest();


            //for (int i = 0; i < selectedContacts.size(); i++) {
            //    resultsArray.add(selectedContacts.get(i));
            //    resultsArray.add("pending");
            //    resultsArray.add("pending");
            //}

            Intent resultsPage = new Intent(this, MultipleResults.class);
            resultsPage.putExtra("results_array", resultsArray);
            startActivity(resultsPage);
        }

    }

    public void selectAll(View view) {
        itemList.clear();

        Button p1_button = (Button) findViewById(R.id.select_all);
        if (p1_button.getText().toString().equals("Select All")) {
            for (int i = 0; i < contactList.size(); i++) {
                Holder itemHolder = new Holder();
                itemHolder.setTitle(contactList.get(i));
                itemHolder.setColor(Color.CYAN);
                itemList.add(itemHolder);
            }
            p1_button.setText("Deselect All");

        } else {
            p1_button.setText("Select All");
            for (int i = 0; i < contactList.size(); i++) {
                Holder itemHolder = new Holder();
                itemHolder.setTitle(contactList.get(i));
                itemHolder.setColor(Color.WHITE);
                itemList.add(itemHolder);
            }
        }

        listAdapter = new CustomListAdapter(this, itemList);
        listView = (ListView) findViewById(R.id.contact_list);
        listView.setAdapter(listAdapter);
    }


    private boolean servicesConnected(Context context) {

        // Check that Google Play services is available
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);

        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {
            // In debug mode, log the status
            Log.d(TAG, "Google Play services is available.");
            // Continue
            return true;
            // Google Play services was not available for some reason
        } else {
            Log.e(TAG, "Services connected - Error");
        }

        Log.e(TAG, "Services connected - Error 2");
        return false;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.map_tab:
                Spinner spinner = (Spinner) findViewById(R.id.spinner);
                int radius = Integer.parseInt(spinner.getSelectedItem().toString());

                double latitude = resultReceiver.getLatLng()[0];
                double longitude = resultReceiver.getLatLng()[1];

                Intent n = new Intent(getApplicationContext(), MapsActivity.class);
                n.putExtra("radius", radius);
                n.putExtra("latitude", latitude);
                n.putExtra("longitude", longitude);
                startActivity(n);
                return true;

            case R.id.app_settings:
                // User chose the "Settings" item, show the app settings UI
                Intent i = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(i);
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }


    private void storeSecretKey(KeyPair keyPair) {
        String secret = keyPair.getSecretKey().getX().toString();
        SharedPreferences prefs = getSharedPreferences("UserCred",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("Secret Key", secret);
        editor.commit();
        Log.d(TAG, "Done. Secret Key stored");
    }


    public static int getTime() {
        Calendar c = Calendar.getInstance();
        int startMili = c.get(Calendar.MILLISECOND);
        int startSecond = c.get(Calendar.SECOND);
        int startMinute = c.get(Calendar.MINUTE);
        int startHour = c.get(Calendar.HOUR);
        int startTime = (startHour * 3600000) + (startMinute * 60000) + (startSecond * 1000) + startMili;
        return startTime;
    }


}

