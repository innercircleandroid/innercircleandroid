package org.example.anonymous.innercircle.network.tasks;

import android.content.Context;

import org.example.anonymous.innercircle.crypto.elgamal.CipherText;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

import javax.crypto.Cipher;

public class ReceiveDataTask extends BackendTask {
    private final ReceiveCallback callback;

    public ReceiveDataTask(Context context, ReceiveCallback callback) {
        super(context);
        this.callback = callback;
    }

    @Override
    protected void communicate(String... params) throws IOException {
        try {
            ArrayList<CipherText> cipherTexts = client.receiveData(context);
            callback.handle(cipherTexts);
        } catch (InterruptedException | JSONException e) {
            e.printStackTrace();
        }
    }

    public static abstract class ReceiveCallback {
        public abstract void handle(ArrayList<CipherText> response);
    }
}
