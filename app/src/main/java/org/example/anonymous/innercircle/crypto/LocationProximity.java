package org.example.anonymous.innercircle.crypto;

import android.util.Log;

import org.example.anonymous.innercircle.crypto.elgamal.CipherText;
import org.example.anonymous.innercircle.crypto.elgamal.ElgamalCrypto;
import org.example.anonymous.innercircle.crypto.elgamal.PublicKey;
import org.example.anonymous.innercircle.crypto.elgamal.SecretKey;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

//import org.bouncycastle.jcajce.provider.asymmetric.ElGamal;

public abstract class LocationProximity {
    private static final String TAG = LocationProximity.class.getName();

    protected final ElgamalCrypto elgamal;
    protected final PublicKey Pk;
    protected int x;
    protected int y;

    public LocationProximity(PublicKey publicKey, int x, int y) {
        Pk = publicKey;
        this.x = x;
        this.y = y;
        this.elgamal = new ElgamalCrypto();
    }

}
