package org.example.anonymous.innercircle.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

import com.google.firebase.iid.FirebaseInstanceId;

import org.example.anonymous.innercircle.R;
import org.example.anonymous.innercircle.network.tasks.RegisterTask;
public class SettingsActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
    }

    public void relog(View view) {
        SharedPreferences prefs = getSharedPreferences("UserCred",
                Context.MODE_PRIVATE);
        String username = prefs.getString("Username", null);

        String token = FirebaseInstanceId.getInstance().getToken();
        if (token != null) {
            new RegisterTask(getApplicationContext()).execute(token, username);
        }

        Intent mainActivityIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(mainActivityIntent);
    }
}