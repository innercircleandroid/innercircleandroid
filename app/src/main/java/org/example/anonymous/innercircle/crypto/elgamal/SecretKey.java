package org.example.anonymous.innercircle.crypto.elgamal;

import java.math.BigInteger;

public class SecretKey {
    private final BigInteger x;

    public SecretKey(BigInteger x) {
        this.x = x;
    }

    public BigInteger getX() {
        return x;
    }
}
