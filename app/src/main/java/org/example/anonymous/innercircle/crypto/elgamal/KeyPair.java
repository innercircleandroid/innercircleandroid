package org.example.anonymous.innercircle.crypto.elgamal;

public class KeyPair {
    private final PublicKey publicKey;
    private final SecretKey secretKey;

    public KeyPair(PublicKey publicKey, SecretKey secretKey) {
        this.publicKey = publicKey;
        this.secretKey = secretKey;
    }

    public PublicKey getPublicKey() {
        return publicKey;
    }

    public SecretKey getSecretKey() {
        return secretKey;
    }
}
