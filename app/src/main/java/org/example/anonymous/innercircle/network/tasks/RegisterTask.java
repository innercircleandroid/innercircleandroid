package org.example.anonymous.innercircle.network.tasks;

import android.content.Context;
import android.os.AsyncTask;

import org.example.anonymous.innercircle.R;
import org.example.anonymous.innercircle.network.Client;

import java.io.IOException;

public class RegisterTask extends AsyncTask<String, Void, Void> {
    private final Context context;

    public RegisterTask(Context context) {
        this.context = context;
    }

    @Override
    protected Void doInBackground(String... params) {
        int hostPort = context.getResources().getInteger(R.integer.backend_port);
        String hostAddress = context.getString(R.string.backend_url);

        Client client = new Client(hostAddress, hostPort);
        try {
            client.connect();

            client.register(params[0], params[1]);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            client.disconnect();
        }
        return null;
    }
}
