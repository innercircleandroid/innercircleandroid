package org.example.anonymous.innercircle.crypto;

import android.util.Log;

import org.example.anonymous.innercircle.crypto.elgamal.CipherText;
import org.example.anonymous.innercircle.crypto.elgamal.PublicKey;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class BobProtocol extends LocationProximity {
    private static final String TAG = BobProtocol.class.getName();
    public BobProtocol(PublicKey pk, int x, int y) {
        super(pk, x, y);
    }

    public ArrayList<CipherText> LessThan(CipherText d, int radius) {
        final long start = System.nanoTime();

        List<Integer> range = SumOfSquares.getSumOfSquares(radius);
        ArrayList<CipherText> result = new ArrayList<>();
        Random rand = new Random();

        for (int i : range) {
            CipherText subtraction = elgamal.add(Pk, d, SumOfSquares.getInverse(i));

            BigInteger random = new BigInteger(Pk.getP().bitCount(), rand);
            CipherText randomized = elgamal.multWithNum(Pk, subtraction, random);
            result.add(randomized);
        }
        Collections.shuffle(result);

        final long end = System.nanoTime();
        Log.i(TAG, String.format("LessThan finished in time='%s' seconds", (end - start) / 1000000000.0d));
        return result;
    }

    public CipherText bobComputes(CipherText a0, CipherText a1, CipherText a2) {
        final long start = System.nanoTime();

        BigInteger xB2 = new BigInteger(String.valueOf(x)).pow(2);
        BigInteger yB2 = new BigInteger(String.valueOf(y)).pow(2);
        BigInteger sqaures = xB2.add(yB2);
        CipherText sub1 = elgamal.add(Pk, a0, elgamal.encryption(Pk, sqaures));
        CipherText crossX = (elgamal.multWithNum(Pk, a1, new BigInteger(String.valueOf(x))));
        CipherText crossY = elgamal.multWithNum(Pk, a2, new BigInteger(String.valueOf(y)));
        CipherText sub2 = elgamal.add(Pk, crossX, crossY);
        CipherText D = elgamal.subtract(Pk, sub1, sub2);

        final long end = System.nanoTime();
        Log.i(TAG, "bobComputes finished in time='" + ((end - start) / 1000000000.0d) + "' seconds");
        return D;
    }
}
