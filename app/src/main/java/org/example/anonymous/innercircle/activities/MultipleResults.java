package org.example.anonymous.innercircle.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.GridView;

import org.example.anonymous.innercircle.R;
import org.example.anonymous.innercircle.activities.MainActivity;

public class MultipleResults extends AppCompatActivity {
    static ArrayAdapter<String> resultsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multiple_results);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        toolbar.setTitle("Watch");
        setSupportActionBar(toolbar);
        resultsAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, MainActivity.resultsArray);
        GridView resultsView = (GridView) findViewById(R.id.resultsView);
        resultsView.setAdapter(resultsAdapter);
        runThread();
        //display selected contacts and range


    }

    private void runThread() {
        new Thread() {
            public void run() {
                int i = 0;
                while (true) {
                    try {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                resultsAdapter.notifyDataSetChanged();
                            }
                        });
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }

    public void returnToMain(View view) {

        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
    }

}
