package org.example.anonymous.innercircle;

public class Holder {

    String user;
    int color;

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public String getTitle() {
        return user;
    }

    public void setTitle(String user) {
        this.user = user;
    }
}