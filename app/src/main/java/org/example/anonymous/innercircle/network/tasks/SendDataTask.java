package org.example.anonymous.innercircle.network.tasks;

import android.content.Context;

import java.io.IOException;

public class SendDataTask extends BackendTask {
    public SendDataTask(Context context) {
        super(context);
    }

    @Override
    protected void communicate(String... params) throws IOException {
        client.sendDataToServer(params[0]);
    }
}
