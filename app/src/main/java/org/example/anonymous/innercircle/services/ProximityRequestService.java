package org.example.anonymous.innercircle.services;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.example.anonymous.innercircle.activities.MainActivity;
import org.example.anonymous.innercircle.crypto.AliceProtocol;
import org.example.anonymous.innercircle.crypto.elgamal.CipherText;
import org.example.anonymous.innercircle.crypto.elgamal.PublicKey;
import org.example.anonymous.innercircle.crypto.elgamal.SecretKey;
import org.example.anonymous.innercircle.network.dto.AliceRequest;
import org.example.anonymous.innercircle.network.tasks.ReceiveDataTask;
import org.example.anonymous.innercircle.network.tasks.SendDataTask;

import java.math.BigInteger;
import java.util.ArrayList;

public class ProximityRequestService {
    private static final String TAG = ProximityRequestService.class.getName();

    private static final int NUM_ITERATIONS = 25;

    private static ProximityRequestService instance;
    private final Context context;
    private final int xA;
    private final int yA;
    private final ArrayList<String> contacts;
    private final int radius;
    private final AliceProtocol aliceProtocol;
    private int iteration;

    public ProximityRequestService(Context context, PublicKey publicKey, int xA, int yA, int radius, ArrayList<String> contacts) {
        this.context = context;
        this.xA = xA;
        this.yA = yA;
        this.radius = radius;
        this.contacts = contacts;

        this.aliceProtocol = new AliceProtocol(publicKey, xA, yA);
        this.iteration = 0;
    }

    public static void setup(Context context, PublicKey publicKey, int xA, int yA, int radius, ArrayList<String> contacts) {
        instance = new ProximityRequestService(context, publicKey, xA, yA, radius, contacts);
    }

    public void performRequest(int radius, int xA, int yA, ArrayList<String> selectedContacts, Context context) {
        int encryptionStart = MainActivity.getTime();
        MainActivity.startTime = System.nanoTime();
        Log.d(TAG, String.format("Request #%d initiated at time='%d'", iteration, MainActivity.startTime));
        Log.i(TAG, "Request with radius='" + radius + "' started");

        AliceRequest alice = new AliceRequest();
        CipherText[] cred = new AliceProtocol(MainActivity.publicKey, xA, yA).generateEncryptedLocation();
        int encryptionEnd = MainActivity.getTime();
        MainActivity.encryptionTime = encryptionEnd - encryptionStart;

        SendDataTask data = new SendDataTask(context);

        SharedPreferences prefs = context.getSharedPreferences("UserCred", Context.MODE_PRIVATE);
        String username = prefs.getString("Username", "");
        data.execute(alice.makeJsonObject(cred, MainActivity.publicKey, radius, selectedContacts, username));//send the Request JsonObject to server
    }

    public void initiateRequest() {
        performRequest(radius, xA, yA, contacts, context);
    }

    public void finalizeRequest() {
        ReceiveDataTask task = new ReceiveDataTask(context, new ReceiveDataTask.ReceiveCallback() {
            @Override
            public void handle(ArrayList<CipherText> response) {
                handleFinalResponse(response);
                if (++iteration < NUM_ITERATIONS) {
                    performRequest(radius, xA, yA, contacts, context);
                }
            }
        });
        task.execute();
    }

    public void handleFinalResponse(ArrayList<CipherText> encResults) {
        String secretKeyString = context.getSharedPreferences("UserCred", Context.MODE_PRIVATE).getString("Secret Key", "");
        SecretKey secret = new SecretKey(new BigInteger(secretKeyString));

        long decryptionStart = System.nanoTime();

        Boolean inRange = aliceProtocol.InProx(encResults, secret);

        long endTime = System.nanoTime();
        long decryptionTime = System.nanoTime() - decryptionStart;
        Log.d(TAG, "Result" + " " + inRange);
        MainActivity.resultsArray.add(0, "" + MainActivity.resultsArray.size() / 3);
        MainActivity.resultsArray.add(1, "" + inRange);
        MainActivity.resultsArray.add(2, "Total: " + toSeconds(endTime - MainActivity.startTime) + "\n" +
                "Encryption: " + String.valueOf(MainActivity.encryptionTime) + "\n" +
                "Decryption: " + toSeconds(decryptionTime));

        Log.d(TAG, "resultlength" + " " + MainActivity.resultsArray.size());

        Log.d(TAG, "Request completed: " + endTime);
        Log.i(TAG, String.format("Request finished in time='%s' seconds", toSeconds(endTime - MainActivity.startTime)));

    }

    private double toSeconds(long t) {
        return t / Math.pow(10.0, 9.0);
    }

    public static ProximityRequestService getInstance() {
        if (instance == null) {
            throw new RuntimeException();
        }

        return instance;
    }
}
