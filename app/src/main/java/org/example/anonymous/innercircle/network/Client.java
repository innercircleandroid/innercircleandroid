package org.example.anonymous.innercircle.network;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.example.anonymous.innercircle.activities.MainActivity;
import org.example.anonymous.innercircle.crypto.AliceProtocol;
import org.example.anonymous.innercircle.crypto.LocationProximity;
import org.example.anonymous.innercircle.crypto.elgamal.CipherText;
import org.example.anonymous.innercircle.crypto.elgamal.SecretKey;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Calendar;

public class Client {

    private static final String TAG = Client.class.getName();
    private String dstAddress;
    private int dstPort;
    private Socket socket = null;
    private BufferedReader input;
    private PrintWriter out;

    // takes the Public Ip , port num
    public Client(String addr, int port) {
        dstAddress = addr;
        dstPort = port;

    }

    // connects to server and initialize the input/output stream
    public void connect() throws IOException {
        InetAddress inetAddress = InetAddress.getByName(dstAddress);
        socket = new Socket(inetAddress.getHostAddress(), dstPort);
        input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
    }

    public int sendDataToServer(String msg) {
        Log.d(TAG, "Msg to Server: " + msg);
        Log.d(TAG, "Msg to Server length: " + msg.length());
        out.println(msg);

        return 1;
    }

    public int getTime() {
        Calendar c = Calendar.getInstance();
        int startMili = c.get(Calendar.MILLISECOND);
        int startSecond = c.get(Calendar.SECOND);
        int startMinute = c.get(Calendar.MINUTE);
        int startHour = c.get(Calendar.HOUR);
        int startTime = (startHour * 3600000) + (startMinute * 60000) + (startSecond * 1000) + startMili;
        return startTime;
    }


    public void register(String token, String name) {
        JSONObject jsonReq = new JSONObject();
        JSONObject jsonObj = new JSONObject();
        try {
            jsonReq.put("Sender_ID", name); // should be replaced by the currect user ID
            jsonReq.put("Reg_ID", token);
            jsonObj.put("Registration", jsonReq);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        out.println(jsonObj.toString());
    }

    public ArrayList<CipherText> receiveData(Context context) throws IOException, InterruptedException, JSONException {
        SharedPreferences prefs = context.getSharedPreferences("UserCred", Context.MODE_PRIVATE);
        String username = prefs.getString("Username", "");

        sendDataToServer("{\"Check\":\"" + username + "\"}");

        String message = input.readLine();
        Log.d(TAG, "Message from server: " + message.length() + "");

        JSONObject answer = new JSONObject(message);
        JSONObject fAnswer = answer.getJSONObject("Answer_Location");
        int timeToEncryptAnswer = fAnswer.getInt("Time");
        Log.d(TAG, "Time: " + timeToEncryptAnswer + " ");
        JSONArray result;

        result = (JSONArray) fAnswer.get("Answer");
        ArrayList<CipherText> encResults = new ArrayList<>();
        for (int i = 0; i < result.length(); i += 2) {
            encResults.add(new CipherText(new BigInteger(result.getString(i)), new BigInteger(result.getString(i + 1))));
        }

        return encResults;
    }

    public void disconnect() {
        if (out != null) {
            out.close();
        }

        try {
            if (input != null) {
                input.close();
            }

            if (socket != null) {
                socket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

