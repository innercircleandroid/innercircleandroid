package org.example.anonymous.innercircle.network.tasks;

import android.content.Context;
import android.os.AsyncTask;

import org.example.anonymous.innercircle.R;
import org.example.anonymous.innercircle.network.Client;

import java.io.IOException;

public abstract class BackendTask extends AsyncTask<String, Void, Void> {
    protected Client client;
    protected Context context;

    public BackendTask(Context context) {
        this.context = context;

        int hostPort = context.getResources().getInteger(R.integer.backend_port);
        String hostAddress = context.getString(R.string.backend_url);

        this.client = new Client(hostAddress, hostPort);
    }


    @Override
    protected Void doInBackground(String... params) {
        try {
            client.connect();
            communicate(params);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            client.disconnect();
        }

        return null;
    }

    protected abstract void communicate(String ... params) throws IOException;
}
