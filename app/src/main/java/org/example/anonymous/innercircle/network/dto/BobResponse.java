package org.example.anonymous.innercircle.network.dto;

import android.support.annotation.NonNull;
import android.util.Log;

import org.example.anonymous.innercircle.activities.MainActivity;
import org.example.anonymous.innercircle.crypto.BobProtocol;
import org.example.anonymous.innercircle.crypto.elgamal.CipherText;
import org.example.anonymous.innercircle.crypto.LocationProximity;
import org.example.anonymous.innercircle.crypto.elgamal.PublicKey;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;

public class BobResponse {
    private static final String TAG = BobResponse.class.getName();


    private String userName;

    public BobResponse(String userName) {
        this.userName = userName;
    }

    @NonNull
    public JSONObject createBobResponse(Map<String, String> cred, int xB, int yB, PublicKey publicKey) throws JSONException {
        int radius = Integer.parseInt(cred.get("Radius"));
        BobProtocol loc = new BobProtocol(publicKey, xB, yB);
        JSONArray bobResult;
        bobResult = new JSONArray();

        CipherText a0 = new CipherText(new BigInteger(cred.get("A0.C0")), new BigInteger(cred.get("A0.C1")));
        CipherText a1 = new CipherText(new BigInteger(cred.get("A1.C0")), new BigInteger(cred.get("A1.C1")));
        CipherText a2 = new CipherText(new BigInteger(cred.get("A2.C0")), new BigInteger(cred.get("A2.C1")));

        CipherText D = loc.bobComputes(a0, a1, a2);
        int startTime = getTime();
        ArrayList<CipherText> result = loc.LessThan(D, radius);
        int endTime = getTime();
        int totalTime = endTime - startTime;

        Log.d(TAG, "totaltime: " + totalTime);

        MainActivity.requests.add(cred.get("Sender_ID"));
        MainActivity.requests.add("" + totalTime);
        for (int i = 0; i < result.size(); i++) {
            bobResult.put(result.get(i).C0.toString());
            bobResult.put(result.get(i).C1.toString());
        }
        JSONObject jsonReq = new JSONObject();
        JSONObject json = new JSONObject();

        jsonReq.put("Sender_ID", userName);// bobs key
        jsonReq.put("Recepient_name", cred.get("Sender_ID"));// alice key which was sent in the request
        jsonReq.put("Answer", bobResult);
        jsonReq.put("Time", totalTime);// results computed by bob
        json.put("Answer_Location", jsonReq);// the tag of the message
        return json;
    }

    public int getTime() {
        Calendar c = Calendar.getInstance();
        int startMili = c.get(Calendar.MILLISECOND);
        int startSecond = c.get(Calendar.SECOND);
        int startMinute = c.get(Calendar.MINUTE);
        int startHour = c.get(Calendar.HOUR);
        int startTime = (startHour * 3600000) + (startMinute * 60000) + (startSecond * 1000) + startMili;
        return startTime;
    }
}
