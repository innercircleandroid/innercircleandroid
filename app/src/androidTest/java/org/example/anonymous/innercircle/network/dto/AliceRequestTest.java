package org.example.anonymous.innercircle.network.dto;

import android.util.Log;

import org.example.anonymous.innercircle.crypto.AliceProtocol;
import org.example.anonymous.innercircle.crypto.BobProtocol;
import org.example.anonymous.innercircle.crypto.LocationProximity;
import org.example.anonymous.innercircle.crypto.SumOfSquares;
import org.example.anonymous.innercircle.crypto.elgamal.CipherText;
import org.example.anonymous.innercircle.crypto.elgamal.ElgamalCrypto;
import org.example.anonymous.innercircle.crypto.elgamal.KeyPair;
import org.example.anonymous.innercircle.crypto.elgamal.PublicKey;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

public class AliceRequestTest extends BaseRequestTest {
    public void testResult() throws JSONException {

        AliceRequest alice = new AliceRequest();
        BobResponse bob = new BobResponse("Bob");

        int xA = 0;
        int yA = 0;

        int xB = 0;
        int yB = 3;

        Log.d("Test ", "" + xB + " " + yB);
        Log.d("Test 1", "" + xA + " " + yA);


        ElgamalCrypto crypto = new ElgamalCrypto();
        KeyPair keyPair = crypto.generateKeys();
        PublicKey publicKey = keyPair.getPublicKey();

        SumOfSquares.initializeSumOfSquares(publicKey);
        // int xB=200000,yB=500000;
        CipherText[] cred = new AliceProtocol(publicKey, xA, yA).generateEncryptedLocation();

        ArrayList<String> names = new ArrayList<>();
        names.add("Alice");
        Map<String, String> fcmMessage = emulateFCM(alice, publicKey, cred, names);
        JSONObject bobResponse = bob.createBobResponse(fcmMessage, xB, yB, publicKey);
        //assertTrue(alice.parseBobResponse(keyPair.getSecretKey(), publicKey, bobResponse.toString(), loc));
    }

}