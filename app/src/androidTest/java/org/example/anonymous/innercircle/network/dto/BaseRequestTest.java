package org.example.anonymous.innercircle.network.dto;

import android.support.annotation.NonNull;

import junit.framework.TestCase;

import org.example.anonymous.innercircle.crypto.elgamal.CipherText;
import org.example.anonymous.innercircle.crypto.elgamal.PublicKey;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class BaseRequestTest extends TestCase {
    @NonNull
    protected Map<String, String> emulateFCM(AliceRequest alice, PublicKey Pk, CipherText[] cipherTexts, ArrayList<String> names) throws JSONException {
        String alice1 = alice.makeJsonObject(cipherTexts, Pk, 500, names, "Alice");
        JSONObject cred = new JSONObject(alice1).getJSONObject("Requests").getJSONObject("Cred");
        Iterator<String> iterator = cred.keys();

        Map<String, String> result = new HashMap<>();
        while (iterator.hasNext()) {
            String key = iterator.next();
            String value = cred.getString(key);
            result.put(key, value);
        }

        return result;
    }
}
