package org.example.anonymous.innercircle;

import junit.framework.TestCase;

import org.example.anonymous.innercircle.crypto.AliceProtocol;
import org.example.anonymous.innercircle.crypto.BobProtocol;
import org.example.anonymous.innercircle.crypto.LocationProximity;
import org.example.anonymous.innercircle.crypto.elgamal.CipherText;
import org.example.anonymous.innercircle.crypto.elgamal.ElgamalCrypto;
import org.example.anonymous.innercircle.crypto.elgamal.KeyPair;
import org.example.anonymous.innercircle.crypto.elgamal.PublicKey;

import java.math.BigInteger;
import java.util.ArrayList;

public class LocationProximityTest extends TestCase {

    public void testInProx() throws Exception {
        ElgamalCrypto crypto = new ElgamalCrypto();
        KeyPair keyPair = crypto.generateKeys();
        PublicKey publicKey = keyPair.getPublicKey();

        int xA = 0;
        int yA = 0;
        int xB = 0;
        int yB = 1;

        CipherText a0 = crypto.encryption(publicKey, new BigInteger(String.valueOf((int) Math.pow(xA, 2) + (int) Math.pow(yA, 2))));
        CipherText a1 = crypto.encryption(publicKey, new BigInteger(String.valueOf(2 * xA)));
        CipherText a2 = crypto.encryption(publicKey, new BigInteger(String.valueOf(2 * yA)));
        BobProtocol locationTest = new BobProtocol(publicKey, xB, yB);
        CipherText D = locationTest.bobComputes(a0, a1, a2);
        ArrayList<CipherText> compute = locationTest.LessThan(D, 4);

        AliceProtocol aliceProtocol = new AliceProtocol(publicKey, xA, yA);
        boolean result = aliceProtocol.InProx(compute, keyPair.getSecretKey());
        assertTrue(result);

    }

    public void testDistance() throws Exception {
        ElgamalCrypto crypto = new ElgamalCrypto();
        KeyPair keyPair = crypto.generateKeys();
        PublicKey publicKey = keyPair.getPublicKey();

        int xA = 0;
        int yA = 0;
        int xB = 0;
        int yB = 1;

        CipherText a0 = crypto.encryption(publicKey, new BigInteger(String.valueOf((int) Math.pow(xA, 2) + (int) Math.pow(yA, 2))));
        CipherText a1 = crypto.encryption(publicKey, new BigInteger(String.valueOf(2 * xA)));
        CipherText a2 = crypto.encryption(publicKey, new BigInteger(String.valueOf(2 * yA)));
        BobProtocol locationTest =new BobProtocol(publicKey, xB, yB);
        CipherText D = locationTest.bobComputes(a0, a1, a2);
        assertEquals(false, crypto.decrypt(publicKey, keyPair.getSecretKey(), D));
    }

    public void testDistance2() throws Exception {
        ElgamalCrypto crypto = new ElgamalCrypto();
        KeyPair keyPair = crypto.generateKeys();
        PublicKey publicKey = keyPair.getPublicKey();

        int xA = 0;
        int yA = 0;
        int xB = 0;
        int yB = 0;

        CipherText a0 = crypto.encryption(publicKey, new BigInteger(String.valueOf((int) Math.pow(xA, 2) + (int) Math.pow(yA, 2))));
        CipherText a1 = crypto.encryption(publicKey, new BigInteger(String.valueOf(2 * xA)));
        CipherText a2 = crypto.encryption(publicKey, new BigInteger(String.valueOf(2 * yA)));
        BobProtocol locationTest =new BobProtocol(publicKey, xB, yB);
        CipherText D = locationTest.bobComputes(a0, a1, a2);
        assertEquals(true, crypto.decrypt(publicKey, keyPair.getSecretKey(), D));


    }

    public void testNotInProx() throws Exception {
        ElgamalCrypto crypto = new ElgamalCrypto();
        KeyPair keyPair = crypto.generateKeys();
        PublicKey publicKey = keyPair.getPublicKey();

        int xA = 0;
        int yA = 0;
        int xB = 0;
        int yB = 5;
        AliceProtocol aliceProtocol = new AliceProtocol(publicKey, xA, yA);

        CipherText a0 = crypto.encryption(publicKey, new BigInteger(String.valueOf((int) Math.pow(xA, 2) + (int) Math.pow(yA, 2))));
        CipherText a1 = crypto.encryption(publicKey, new BigInteger(String.valueOf(2 * xA)));
        CipherText a2 = crypto.encryption(publicKey, new BigInteger(String.valueOf(2 * yA)));
        BobProtocol locationTest = new BobProtocol(publicKey, xB, yB);
        CipherText D = locationTest.bobComputes(a0, a1, a2);
        ArrayList<CipherText> compute = locationTest.LessThan(D, 4);

        boolean result = aliceProtocol.InProx(compute, keyPair.getSecretKey());
        assertFalse(result);

    }
}