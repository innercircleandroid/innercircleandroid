package org.example.anonymous.innercircle.network.dto;

import org.example.anonymous.innercircle.crypto.AliceProtocol;
import org.example.anonymous.innercircle.crypto.BobProtocol;
import org.example.anonymous.innercircle.crypto.LocationProximity;
import org.example.anonymous.innercircle.crypto.elgamal.CipherText;
import org.example.anonymous.innercircle.crypto.elgamal.ElgamalCrypto;
import org.example.anonymous.innercircle.crypto.elgamal.KeyPair;
import org.example.anonymous.innercircle.crypto.elgamal.PublicKey;
import org.json.JSONObject;

import java.util.ArrayList;

public class BobResponseTest extends BaseRequestTest {

    public void testCreateBobResponse() throws Exception {
        BobResponse bob = new BobResponse("Bob");
        AliceRequest alice = new AliceRequest();
        ElgamalCrypto crypto = new ElgamalCrypto();
        KeyPair keyPair = crypto.generateKeys();
        PublicKey publicKey = keyPair.getPublicKey();
        int xA = 0, yA = 1;
        CipherText[] cred = new AliceProtocol(publicKey, xA, yA).generateEncryptedLocation();
        ArrayList<String> names = new ArrayList<>();
        names.add("Alice");
        JSONObject bobResponse = bob.createBobResponse(emulateFCM(alice, publicKey, cred, names), 0, 3, publicKey);

        // assertEquals(160,bobResponse.length());
    }

}