#!/bin/bash

source phones.sh

APK_PATH=$(pwd)/app/build/outputs/apk/app-debug.apk
TARGET_PATH=/data/local/tmp/org.example.anonymous.innercircle

bash gradlew clean assemble

echo "Copying APKs..."
adb -s $PHONE_1 push $APK_PATH $TARGET_PATH
adb -s $PHONE_2 push $APK_PATH $TARGET_PATH

echo "Installing APKs..."
adb -s $PHONE_1 shell pm install -r "$TARGET_PATH"
adb -s $PHONE_2 shell pm install -r "$TARGET_PATH"

echo "Launching apps..."
ACTIVITY=org.example.anonymous.innercircle/org.example.anonymous.innercircle.activities.LoginActivity
adb -s $PHONE_1 shell am start -n "$ACTIVITY" -a android.intent.action.MAIN -c android.intent.category.LAUNCHER
adb -s $PHONE_2 shell am start -n "$ACTIVITY" -a android.intent.action.MAIN -c android.intent.category.LAUNCHER

